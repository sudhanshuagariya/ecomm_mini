package com.example.demo.services;

import com.example.demo.model.Login;
import com.example.demo.model.Recharge;
import com.example.demo.model.User;
import com.example.demo.repositories.UserRepository;
import com.example.demo.repositories.WalletRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

//import static org.graalvm.compiler.options.OptionType.User;
import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private WalletRepository walletRepository;
    @Mock
    private UserService userService;
    @InjectMocks
    private UserServiceImpl userServiceImpl;
    @Test
    @DisplayName("Test should pass")
    void rechargeWalletOne() {
        String mobOne = "7869453212";
        User user = new User("s","a",mobOne,"sud@gmail.com","123");
        Mockito.when(userRepository.findByMobileNumber(mobOne)).thenReturn(Optional.of(user));
        Boolean actualResponse = userServiceImpl.createMyUser(user);
        Assertions.assertEquals(false,actualResponse,"It should be false");
    }
    @Test
    @DisplayName("Test should pass")
    void rechargeWalletThree() {
        String mobOne = "7869453212";
        User user = new User("s","a",mobOne,"sud@gmail.com","123");
        Mockito.when(userRepository.findByMobileNumber(mobOne)).thenReturn(Optional.empty());
        Boolean actualResponse = userServiceImpl.createMyUser(user);
        Assertions.assertEquals(true,actualResponse,"It should be true");
    }
    @Test
    @DisplayName("Test should pass")
    void rechargeWalletEight() {
        String mobOne = "7869453212";
        String password= "123";
        Login login = new Login(mobOne,password);
        Mockito.when(userRepository.findByMobileNumber(mobOne)).thenReturn(Optional.empty());
        Boolean actualResponse = userServiceImpl.loginMyUser(login);
        Assertions.assertEquals(false,actualResponse,"It should be true");
    }
    @Test
    @DisplayName("Test should pass")
    void rechargeWalletNine() {
        String mobOne = "7869453212";
        String password= "123";
        User user = new User("s","a",mobOne,"sud@gmail.com","123");
        Login login = new Login(mobOne,password);
        Mockito.when(userRepository.findByMobileNumber(mobOne)).thenReturn(Optional.of(user));
        Boolean actualResponse = userServiceImpl.loginMyUser(login);
        Assertions.assertEquals(true,actualResponse,"It should be true");
    }
}