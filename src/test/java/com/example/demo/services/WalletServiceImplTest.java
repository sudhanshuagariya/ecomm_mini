package com.example.demo.services;

import com.example.demo.model.Recharge;
import com.example.demo.model.Transaction;
import com.example.demo.model.Transfer;
import com.example.demo.model.Wallet;
import com.example.demo.repositories.UserRepository;
import com.example.demo.repositories.WalletRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
public class WalletServiceImplTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private WalletRepository walletRepository;
    @Mock
    private UserService userService;
    @InjectMocks
    private WalletServiceImpl walletServiceImpl;


    @Test
    @DisplayName("Test should pass")
    void rechargeWalletOne() {
        String mobOne = "7869453212";
        Recharge recharge = new Recharge(mobOne,200);
        Mockito.when(walletRepository.findByMobileNumber(mobOne)).thenReturn(Optional.empty());
        Boolean actualResponse = walletServiceImpl.rechargeWallet(recharge);
        Assertions.assertEquals(false,actualResponse,"It should be false");
    }
    @Test
    @DisplayName("Test should pass")
    void rechargeWalletTen() {
        String mobOne = "7869453212";
        Transaction t1 = new Transaction("credit",200,"bank",mobOne);
        ArrayList<Transaction> listOne = new ArrayList<>();
        listOne.add(t1);
        Wallet wallet = new Wallet(mobOne,400,listOne);
        Recharge recharge = new Recharge(mobOne,200);
        Mockito.when(walletRepository.findByMobileNumber(mobOne)).thenReturn(Optional.of(wallet));
        Boolean actualResponse = walletServiceImpl.rechargeWallet(recharge);
        Assertions.assertEquals(true,actualResponse,"It should be true");
    }
//    @Test
//    @DisplayName("Test should pass")
//    void rechargeWalletTwo() {
//        String mobOne = "7869453212";
//        double balance = 400;
//        Transaction t1 = new Transaction("credit",200,"9009571910","7869453212");
//        ArrayList<Transaction> listOne = new ArrayList<>();
//        listOne.add(t1);
//        Wallet walletOne = new Wallet("7869453212",400,listOne);
//        Wallet walletTwo = new Wallet("7869453212",400,listOne);
////        Recharge recharge = new Recharge("7869453212",200);
//        Mockito.when(walletRepository.save(wallet)).thenReturn(wallet);
//        Boolean actualResponse = walletServiceImpl.rechargeWallet(recharge);
//        Assertions.assertEquals(false,actualResponse,"It should be true");
//    }
    @Test
    @DisplayName("Test should pass")
    void transferAmountOne() {
    String phoneOne = "7869453212";
    String phoneTwo = "7654372840";
    double balance = 400;
    Transaction t1 = new Transaction("credit",200,phoneOne,phoneTwo);
    Transaction t2 = new Transaction("credit",200,phoneTwo,phoneOne);
    ArrayList<Transaction> listOne = new ArrayList<>();
    listOne.add(t1);
    Wallet walletOne = new Wallet(phoneOne,400,listOne);
    Wallet walletTwo = new Wallet(phoneTwo,400,listOne);
    Transfer transfer = new Transfer(phoneOne,phoneTwo,200);
    Mockito.when(walletRepository.findByMobileNumber(walletOne.getMobileNumber())).thenReturn(Optional.of(walletOne));
    Mockito.when(walletRepository.findByMobileNumber(walletTwo.getMobileNumber())).thenReturn(Optional.of(walletTwo));
    Boolean myResponseOne = walletServiceImpl.transferAmount(transfer);
    Assertions.assertEquals(200,walletTwo.getBalance(),"It should be false");
    }
//    @Test
//    @DisplayName("Test should pass")
//    void viewStatementOne() {
//        String mobOne = "7869453212";
//        double balance = 400;
//        Transaction t1 = new Transaction("credit",200,"9009571910","7869453212");
//        ArrayList<Transaction> listOne = new ArrayList<>();
//        listOne.add(t1);
//        Wallet wallet = new Wallet("7869453212",400,listOne);
//        String mobileNumber = "7869453212";
//        Mockito.when(walletRepository.findByMobileNumber(mobileNumber)).thenReturn(Optional.empty());
//        Wallet actualResponse = walletServiceImpl.viewStatement(mobileNumber);
//        Assertions.assertEquals(null,actualResponse,"It should be false");
//    }
//
}