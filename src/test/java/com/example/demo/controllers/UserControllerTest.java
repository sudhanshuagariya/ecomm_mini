package com.example.demo.controllers;

import com.example.demo.model.User;
import com.example.demo.repositories.UserRepository;
import com.example.demo.repositories.WalletRepository;
import com.example.demo.services.UserService;
import com.example.demo.services.UserServiceImpl;
import com.example.demo.services.WalletServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserController.class)
class UserControllerTest {
    @MockBean
    private UserService userService;
    @MockBean
    private UserServiceImpl userServiceImpl;
    @MockBean
    private UserRepository userRepository;
    @Autowired
    private MockMvc mockMvc;

//    @Test
//    @DisplayName("Should list all the users")
//    public void shouldListAllUser() throws Exception {
//        User userOne = new User("A","B","9826922406","sud@gmail.com","123");
//        User userTwo = new User("C","D","9826922407","sushant@gmail.com","123");
//        Mockito.when(userRepository.findAll()).thenReturn(Arrays.asList(userOne,userTwo));
//        mockMvc.perform(MockMvcRequestBuilders.get("/users"))
//                .andExpect(MockMvcResultMatchers.status().is(200))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.size()",is()));
//    }
//    @Test
//    public void testGetExample() throws Exception {
//        List<User> users = new ArrayList<>();
//        User userOne = new User("A","B","9826922406","sud@gmail.com","123");
//        User userTwo = new User("C","D","9826922407","sushant@gmail.com","123");
//        users.add(userOne);
//        users.add(userTwo);
//        Mockito.when(userRepository.findAll()).thenReturn(users);
//        mockMvc.perform(MockMvcRequestBuilders.get("/users"))
//                .andExpect(MockMvcResultMatchers.status().isOk());
//    }
}