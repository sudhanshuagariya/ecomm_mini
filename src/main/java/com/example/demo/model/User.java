package com.example.demo.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "users")
public class User {
    @NotNull(message = "First Name cant be null")
    private String firstName;
    @NotNull(message = "Last Name cant be null")
    private String lastName;
    @Id@NotNull(message = "First Name cant be null")
    private String mobileNumber;
    @NotNull(message = "First Name cant be null")
    private String email;
    @NotNull(message = "First Name cant be null")
    private String password;

}
