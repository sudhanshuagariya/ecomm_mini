package com.example.demo.controllers;


import com.example.demo.model.Login;
import com.example.demo.model.Transaction;
import com.example.demo.model.User;
import com.example.demo.model.Wallet;
import com.example.demo.repositories.UserRepository;
import com.example.demo.repositories.WalletRepository;
import com.example.demo.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("http://localhost:3000")
public class UserController {
    @Autowired
    public UserRepository userRepository;

    @Autowired
    public WalletRepository walletRepository;

    @Autowired
    public UserService userService;

    @GetMapping(value = "/users")
    public ResponseEntity<?> getAllUsers(){
        List<User> user= userRepository.findAll();
        if(user.size()>0)
            return  new ResponseEntity<List<User>>(user, HttpStatus.OK);
        else
            return new ResponseEntity<>("No Users Available",HttpStatus.NOT_FOUND);
    }
    @PostMapping(value = "/users")
    public ResponseEntity<?> createUser(@RequestBody User user){
        boolean result = userService.createMyUser(user);
        if(result){
            return new ResponseEntity<>(user,HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Mobile Number Already Registered",HttpStatus.OK);
        }
    }
    @PostMapping(value = "/login")
    public ResponseEntity<?> loginMyUser(@RequestBody Login login){
        boolean resultOne = userService.loginMyUser(login);
        if(resultOne){
            return new ResponseEntity<>(login,HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Invalid Credentials",HttpStatus.OK);
        }
    }

}
