package com.example.demo.controllers;


import com.example.demo.model.Recharge;
import com.example.demo.model.Transfer;
import com.example.demo.model.User;
import com.example.demo.model.Wallet;
import com.example.demo.repositories.UserRepository;
import com.example.demo.repositories.WalletRepository;
import com.example.demo.services.UserService;
import com.example.demo.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("http://localhost:3000")
public class WalletController {
    @Autowired
    public UserRepository userRepository;

    @Autowired
    public WalletRepository walletRepository;

    @Autowired
    public UserService userService;
    @Autowired
    public WalletService walletService;

    @PostMapping(value = "/recharge")
    public ResponseEntity<?> rechargeWallet(@RequestBody Recharge recharge) {
        Boolean resultTwo = walletService.rechargeWallet(recharge);
        if(resultTwo){
            return new ResponseEntity<>(recharge, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Invalid credentials",HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    @PostMapping(value = "/transfer")
    public ResponseEntity<?> transferAmount(@RequestBody Transfer transfer) {
        Boolean resultThree = walletService.transferAmount(transfer);
        if(resultThree){
            return new ResponseEntity<>(transfer,HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Invalid Credentials",HttpStatus.OK);
        }
    }
//    @GetMapping(value = "/wallet/{mobileNumber}")
//    public ResponseEntity<?> viewMyStatement(@PathVariable("mobileNumber") String mobileNumber){
//        Wallet resultFour = walletService.viewStatement(mobileNumber);
//        if(resultFour.equals(null)){
//            return new ResponseEntity<>("Mobile Number not Found ",HttpStatus.OK);
//        }
//        else{
//            return new ResponseEntity<>(resultFour,HttpStatus.OK);
//        }
//    }
@GetMapping(value = "/wallet/{mobileNumber}")
public ResponseEntity<?> viewMyStatement(@PathVariable("mobileNumber") String mobileNumber){
    Optional<Wallet> walletOptionalThree = walletRepository.findByMobileNumber(mobileNumber);
    if(walletOptionalThree.isPresent()){
        Wallet wallet = walletOptionalThree.get();
        return new ResponseEntity<>(wallet,HttpStatus.OK);
    }
    else{
        return new ResponseEntity<>("Mobile Number not Found ",HttpStatus.OK);
    }
}
    @GetMapping(value = "/wallet")
    public ResponseEntity<?> getAllWallet(){
        List<Wallet> wallet= walletRepository.findAll();
        if(wallet.size()>0)
            return  new ResponseEntity<List<Wallet>>(wallet, HttpStatus.OK);
        else
            return new ResponseEntity<>("No Users Available",HttpStatus.NOT_FOUND);
    }

}
