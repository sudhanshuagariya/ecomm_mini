package com.example.demo.repositories;

import com.example.demo.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User,String> {
    @Query("{'mobileNumber':?0}")
    Optional<User> findByMobileNumber(String mobileNumber);
}
