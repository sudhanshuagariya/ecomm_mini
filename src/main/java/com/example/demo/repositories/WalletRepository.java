package com.example.demo.repositories;

import com.example.demo.model.Wallet;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface WalletRepository extends MongoRepository<Wallet,String> {
    @Query("{'mobileNumber':?0}")
    Optional<Wallet> findByMobileNumber(String mobileNumber);
}
