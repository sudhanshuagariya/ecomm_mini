package com.example.demo.services;

import com.example.demo.model.Recharge;
import com.example.demo.model.Transfer;
import com.example.demo.model.User;
import com.example.demo.model.Wallet;

public interface WalletService {
    public Boolean rechargeWallet(Recharge recharge);
    public Boolean transferAmount(Transfer transfer);
//    public Wallet viewStatement(String mobileNumber);
}
