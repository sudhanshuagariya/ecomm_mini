package com.example.demo.services;

import com.example.demo.model.Login;
import com.example.demo.model.User;

public interface UserService {
    public Boolean createMyUser(User user);
    public Boolean loginMyUser(Login login);
}
