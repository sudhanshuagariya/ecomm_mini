package com.example.demo.services;

import com.example.demo.model.*;
import com.example.demo.repositories.UserRepository;
import com.example.demo.repositories.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WalletServiceImpl implements WalletService{
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WalletRepository walletRepository;
    @Autowired
    private UserService userService;

    @Override
    public Boolean rechargeWallet(Recharge recharge) {
        String mobOne= recharge.getMobileNumberOne();
        double bal = recharge.getAmountOne();
        Optional<Wallet> walletOptional = walletRepository.findByMobileNumber(mobOne);
        if(walletOptional.isPresent()) {
            Wallet w = walletOptional.get();
            w.setBalance(w.getBalance() + bal);
            Transaction t1= new Transaction("debit",bal,"bank",w.getMobileNumber());
            w.getList().add(t1);
            walletRepository.save(w);
            return true;
        }
        else
        {
            return false;
        }
    }
    @Override
    public Boolean transferAmount(Transfer transfer){
        String phoneOne = transfer.getMobOne();
        String phoneTwo = transfer.getMobTwo();
        double amoOne = transfer.getAmo();
        Optional<Wallet> walletOptionalOne = walletRepository.findByMobileNumber(phoneOne);
        Optional<Wallet> walletOptionalTwo = walletRepository.findByMobileNumber(phoneTwo);
        if(walletOptionalOne.isPresent() && walletOptionalTwo.isPresent()){
            Wallet wOne = walletOptionalOne.get();
            Wallet wTwo = walletOptionalTwo.get();
            wOne.setBalance(wOne.getBalance()+amoOne);
            wTwo.setBalance(wTwo.getBalance()-amoOne);
            Transaction tOne= new Transaction("debit",amoOne,wTwo.getMobileNumber(),wOne.getMobileNumber());
            wOne.getList().add(tOne);
            Transaction tTwo= new Transaction("credit",amoOne,wOne.getMobileNumber(),wTwo.getMobileNumber());
            wTwo.getList().add(tTwo);
            walletRepository.save(wOne);
            walletRepository.save(wTwo);
            return true;
        }
        else{
            return false;
        }

    }
//    @Override
//    public Wallet viewStatement(String mobileNumber){
//        Optional<Wallet> walletOptionalThree = walletRepository.findByMobileNumber(mobileNumber);
//        Wallet wallet = walletOptionalThree.get();
//        if(walletOptionalThree.isPresent()){
//            return wallet;
//        }
//        else{
//            return null;
//        }
//    }
}
