package com.example.demo.services;

import com.example.demo.model.Login;
import com.example.demo.model.Transaction;
import com.example.demo.model.User;
import com.example.demo.model.Wallet;
import com.example.demo.repositories.UserRepository;
import com.example.demo.repositories.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{


    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WalletRepository walletRepository;
    @Override
    public Boolean createMyUser(User user){
        String mobileNumber= user.getMobileNumber();
        Optional<User> userOptional = userRepository.findByMobileNumber(mobileNumber);
        if(userOptional.isPresent()){
            return false;
        }
        else{
            userRepository.save(user);
            Wallet w = new Wallet();
            w.setMobileNumber(user.getMobileNumber());
            w.setBalance(0);
            ArrayList<Transaction> t = new ArrayList<Transaction>();
            w.setList(t);
            walletRepository.save(w);
            return true;
        }

    }
    @Override
    public Boolean loginMyUser(Login login){
        String mobileNumber = login.getMobileNumber();
        String password = login.getPassword();
        Optional<User> userOptional = userRepository.findByMobileNumber(mobileNumber);
        if(userOptional.isPresent()){
            User myUser = userOptional.get();
            String myPassword= myUser.getPassword();
                if(myPassword.equals(password)){
                    return true;
                    }
                else{
                    return false;
                    }
        }
        else{
            return false;
        }
    }
}
